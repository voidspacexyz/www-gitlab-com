---
layout: markdown_page
title: University
---

## What is GitLab University

Version control will be the standard for creation of any type of media in the
future. Today it's still pretty hard to master.

GitLab University has as a goal to teach Git, GitLab and everything that relates
to that to anyone willing to listen and participate.

## [Classes](/university/classes)

Right now, there is a GitLab University class every Thursday at 5PM UTC.
To sign up, send Job a message.

If you're interested in any of these, would like to teach in one of them or
participate or help in any way, please contact Job or submit a merge request.

- [View all classes](/university/classes)

## Previous Topics

- [Innersourcing](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
- [GitLab Ecosystem](https://docs.google.com/presentation/d/1vCU-NbZWz8NTNK8Vu3y4zGMAHb5DpC8PE5mHtw1PWfI/edit)
- [GitLab Basics](http://doc.gitlab.com/ce/gitlab-basics/README.html)
- [GitLab Introduction to Terminology](https://about.gitlab.com/2015/05/18/simple-words-for-a-gitlab-newbie/)
- [Intro to Git](https://www.codeschool.com/account/courses/try-git)
- [Create and Add your SHH key to GitLab](https://www.youtube.com/watch?v=54mxyLo3Mqk)
- [Creating a new project in GitLab](https://www.youtube.com/watch?v=7p0hrpNaJ14)
- [Issue and Merge Request](https://www.youtube.com/watch?v=raXvuwet78M)

## Upcoming Topics

Please submit a merge request for suggestions.

- HA vs. Scalability
- Groups, Projects, Repositories
- JIRA and Jenkins integrations in GitLab

## Resources

- [GitLab documentation](http://doc.gitlab.com/)
- [Innersourcing article](https://about.gitlab.com/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/)
- [Platzi training course](https://courses.platzi.com/courses/git-gitlab/)
- [GitLab flow](http://doc.gitlab.com/ee/workflow/gitlab_flow.html)
- [Sales Onboarding materials](https://about.gitlab.com/handbook/sales-onboarding/)
- [GitLab Direction](https://about.gitlab.com/direction/)
- [GitLab compared to other tools](https://about.gitlab.com/comparison/)
- [Create a GitLab Account](https://courses.platzi.com/classes/git-gitlab/concepto/first-steps/create-an-account-on-gitlab/material/)
- [Gitlab Workshop Part 1: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-1/material/)
- [GitLab Workshop Part 2: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-23370/material/)
- [Gitlab Workshop Part 3: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-3/material/)
- [Gitlab Workshop Part 4: Basics of Git and Gitlab](https://courses.platzi.com/classes/git-gitlab/concepto/part-1/part-4/material/)
- [Demo of GitLab.com](https://www.youtube.com/watch?v=WaiL5DGEMR4)
- [Client Demo of GitLab with Job and Haydn](https://gitlabmeetings.webex.com/cmp3000/webcomponents/jsp/docshow/closewindow.jsp)
- [What is GitLab OmniBus?](https://www.youtube.com/watch?v=XTmpKudd-Oo)
- [How to install GitLab with Omnibus](https://www.youtube.com/watch?v=Q69YaOjqNhg)
- [Managing permissions within EE](https://www.youtube.com/watch?v=DjUoIrkiNuM)
- [GitLab Flow](https://about.gitlab.com/2014/09/29/gitlab-flow/)
- [GitLab Flow vs. Forking in GitLab](https://www.youtube.com/watch?v=UGotqAUACZA)
- [Managing LDAP, Active Directory introduction with GitLab](https://www.youtube.com/watch?v=HPMjM-14qa8)
- [Amazon's transition to Continuous Delivery](https://www.youtube.com/watch?v=esEFaY0FDKc)